// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyACfqb671J57IA1bp10MGrwroscYPva9dk",
    authDomain: "exam-shiran.firebaseapp.com",
    projectId: "exam-shiran",
    storageBucket: "exam-shiran.appspot.com",
    messagingSenderId: "292576467218",
    appId: "1:292576467218:web:ba41eb5070ac70b4343bcb"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
