import { Student } from './../interfaces/student';
import { StudentsService } from './../students.service';
import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {

  math:number;
  psych:number;
  pay:string;
  paid:Object[] = [{id:1,name:'true'},{id:2,name:'false'}]
  result:number; 
  id:string; 
  @Output() add = new EventEmitter<Student>();
  @Output() closeAdd = new EventEmitter<null>();
  


  constructor(public studentsService:StudentsService) { }
  
  predict(){
    console.log()
    this.studentsService.predict(this.math, this.psych, this.pay).subscribe(
      res => {
        console.log(res);
        if(result>'0.5'){
          var result = 'Does not fall off'
        }
        else{
          var result = "Fall off"
        }
        result = result;
      }
    )
  }

  updateParent(){
    let student:Student = {id:this.id, math:this.math, psych:this.psych};
    this.add.emit(student); 
  }


  tellParentToClose(){
    this.closeAdd.emit(); 
  }

  ngOnInit(): void {
  }

}
