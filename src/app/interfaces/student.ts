export interface Student {
    userId?:string, 
    id:string,
    math:number,
    psych:number,
    pay?:string;

}
