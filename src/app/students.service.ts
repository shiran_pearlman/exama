import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {
  private url = "https://07jzgkvd1l.execute-api.us-east-1.amazonaws.com/beta"
  studentCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users'); 

  public getStudents(userId){
    this.studentCollection = this.db.collection(`users/${userId}/students`); 
    return this.studentCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
    
  } 


  predict(math:number, psych:number, pay:string){
    let json = {
      "math":math,
      "psych":psych,
      "pay":pay
    }
    let result = JSON.stringify(json);
    return this.http.post<any>(this.url, result).pipe(
      map(res => {
        console.log(res);
        const final:string = res.result;
        console.log(final);
        return final; 
      })
    )

  }

  deleteStudent(userId:string, id:string){
    this.db.doc(`users/${userId}/students/${id}`).delete(); 
  } 

  addStudent(userId:string,math:number,psych:number){
    const student = {math:math, psych:psych}; 
    this.userCollection.doc(userId).collection('students').add(student);
  }

  constructor(private db:AngularFirestore, private http:HttpClient, private auth:AuthService) { }
}
