import { StudentsService } from './../students.service';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Student } from '../interfaces/student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  students$;
  userId:string;
  math:number;
  psych:number;
  students:Student[];


  constructor(public authService:AuthService, private studentsService:StudentsService) { }

  deleteStudent(id:string){
    this.studentsService.deleteStudent(this.userId,id); 
  }

  add(student:Student){
    this.studentsService.addStudent(this.userId, student.math, student.psych); 
  }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.students$ = this.studentsService.getStudents(this.userId);  
      }
    )

  }

}
